\begin{titlepage}
    \centering
    \vspace*{0.5 cm}
    \textsc{\LARGE Università di Bologna}\\[2.0 cm] % University Name
    \textsc{\Large Programmazione ad Oggetti}\\[0.5 cm] % Course Code

    \vspace*{2 cm}

    \rule{\linewidth}{0.2 mm} \\[0.4 cm]
    { \huge \bfseries Project Name }\\
    \rule{\linewidth}{0.2 mm} \\[1.5 cm]

    \vspace*{5 cm}

    \begin{minipage}{0.4\textwidth}
    \end{minipage}~
    \begin{minipage}{0.4\textwidth}
        \begin{center} \large
            \emph{Realizzata da:} \\
            \vspace*{2mm}
            name surname \\
            name surname \\
        \end{center}
    \end{minipage}\\[2 cm]
\end{titlepage}
